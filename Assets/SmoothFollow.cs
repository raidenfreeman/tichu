﻿using UnityEngine;
using System.Collections;
using System;

/// <summary>
/// Smoothly follows a target object
/// </summary>
public class SmoothFollow : MonoBehaviour
{
    /// <summary>
    /// Reference to the active follow coroutine. Null if it's inactive.
    /// </summary>
    private Coroutine followCoroutine;

    /// <summary>
    /// Occurs when the object starts following a target
    /// </summary>
    public event EventHandler StartedFollowing;

    /// <summary>
    /// Occurs when the object stops following a target
    /// </summary>
    public event EventHandler StoppedFollowing;

    /// <summary>
    /// Starts following the target object, starting fast and slowing as it approaches
    /// </summary>
    /// <param name="target">The transform of the object to follow</param>
    /// <param name="lerpStep"></param>
    /// <param name="lookAtTarget">Whether or not to rotate the object, so that it looks at the target</param>
    /// <returns>True if successful, false if it's already following something</returns>
    public bool TryStartFollowingTarget(Transform target, float lerpStep, bool lookAtTarget)
    {
        //if it's already following something, fail fast
        if (followCoroutine != null)
        {
            return false;
        }

        //TODO replace lerpStep with duration: How long it would take to reach the target, if it remained still. The lower it is, the faster it follows the target.
        var startedFollowingEventReference = StartedFollowing;
        if (startedFollowingEventReference != null)
        {
            startedFollowingEventReference.Invoke(this, EventArgs.Empty); //notify people of starting to follow
        }
        followCoroutine = StartCoroutine(SmoothFollowCoroutine(this.transform, target, lerpStep, true));//get a reference to the coroutine, in order to be able to stop it.
        return true;
    }

    /// <summary>
    /// Stop following the target
    /// </summary>
    public void StopFollowing()
    {
        if (followCoroutine != null)
        {
            StopCoroutine(followCoroutine);
            followCoroutine = null;
        }
        var eventHandler = StoppedFollowing;
        if (eventHandler != null)
        {
            StoppedFollowing.Invoke(this, EventArgs.Empty);
        }
    }

    /// <summary>
    /// Starts following the mouse, starting fast and slowing as it approaches
    /// </summary>
    /// <param name="lerpStep"></param>
    /// <param name="lookAtMouse">Whether or not to rotate the object, so that it looks at the mouse</param>
    /// <param name="lookAtDepth">If <paramref name="lookAtMouse"/> is set to true, this represents the Z position of the mouse. Otherwise it doesn't matter.</param>
    public void StartFollowingMouse(float lerpStep, bool lookAtMouse, float lookAtDepth)
    {
    }

    /// <summary>
    /// Checks if the two transforms are close enough
    /// </summary>
    /// <param name="objectLocation">First object</param>
    /// <param name="targetLocation">Second object</param>
    /// <param name="threshold">The squared threshold of their minimum distance to be considered close</param>
    /// <returns>True if they are close enough</returns>
    bool areCloseEnough(Vector3 objectLocation, Vector3 targetLocation, float threshold)
    {
        return (objectLocation - targetLocation).sqrMagnitude < threshold;
    }

    IEnumerator SmoothFollowCoroutine(Transform followingObject, Transform target, float duration, bool lookAtTarget)
    {
        const float timeStep = 0.01f;
        const float proximityThreshold = 0.01f;
        float elapsedTime = 0f;
        float lerpStep = duration;

        //while they're not very close
        while (true)
        {
            if (!areCloseEnough(followingObject.position, target.position, proximityThreshold))
            {
                //elapsedTime += timeStep;
                //lerpStep = duration / elapsedTime;

                //lerp the object towards the target
                followingObject.position = Vector3.Lerp(followingObject.position, target.position, lerpStep);

                if (lookAtTarget)
                {
                    followingObject.LookAt(target);
                }
                //force limits to rotation
                //followinObject.localRotation = Utility.ClampRotation(followingObject.localRotation, -60, 60, -40, 40, 0, 0);
            }
            else
            {
                //when they get close enough
                //set 
                followingObject.position = target.position;

                //followingObject.rotation = Quaternion.identity;
            }
            yield return new WaitForSeconds(timeStep);
        }

    }

}
