﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class DeckManager : MonoBehaviour
{

    #region Singleton
    //make it a singleton
    private static DeckManager instance;

    public static DeckManager Instance
    {
        get
        {
            if (instance == null)
                instance = GameObject.FindObjectOfType<DeckManager>();
            return instance;
        }
    }
    #endregion


    List<Card> deck;

    public GameObject cardPrefab;

    public Sprite[] CardSprites;

    public Sprite[] SpecialCardSprites;

    public Transform canvas;

    public Hand player1Hand;

    // Use this for initialization
    void Start()
    {
        //testing
        RuleVariables.canvasRectTransform = transform.parent.GetComponent<RectTransform>();

        deck = BuildDeck();
        if (deck == null)
        {
            Debug.Log("Deck is null!");
            return; //replace with exception
        }
        deck.Shuffle();
        if(player1Hand==null)
        {
            Debug.Log("player hand missing");
            return;
        }
        player1Hand.AddCards(deck);
    }

    List<Card> BuildDeck()
    {
        if (cardPrefab == null)
        {
            Debug.Log("Card Prefab not set!");
            return null; //replace with exception
        }

        int totalCards = RuleVariables._totalCards;
        if (totalCards <= 0)
        {
            Debug.Log("Invalid _totalCards variable");
            throw new System.ArgumentException();
        }

        List<Card> cardList = new List<Card>();
        AddRegularCards(cardList);
        AddSpecialCards(cardList);

        return cardList;
    }

    void AddRegularCards(List<Card> cardList)
    {
        int totalCards = RuleVariables._totalRegularCards;
        int totalSuits = RuleVariables._totalRegularCardSuits;
        for (int i = 0; i < totalCards; i++)
        {
            Card currentCard = ((GameObject)Instantiate(cardPrefab)).GetComponent<Card>();
            int newCardID = i;
            int newCardNumericalValue = i % (totalCards / totalSuits) + 1;
            newCardNumericalValue = newCardNumericalValue == 1 ? 14 : newCardNumericalValue;
            /* for example for i=16, 16 % (52/4) + 1 = 16 % 14 + 1 = 3
            /*usually there are 52 cards and 4 suits => 13 cards per suit. We % 14 and want values 1-13, so we + 1.*/
            CardSuit newCardSuit = (CardSuit)(i / RuleVariables._cardsPerSuit);//THIS LOOKS BAD
            currentCard.data = new CardData(newCardID, newCardNumericalValue, newCardSuit);
            try
            {
                currentCard.ChangeSprite(CardSprites.ElementAt(i));
            }
            catch (System.ArgumentOutOfRangeException)
            {
                Debug.LogError("Card Sprite Array out of bounds");
            }

            currentCard.transform.SetParent(canvas);
            currentCard.transform.position = transform.position;

            cardList.Add(currentCard);
        }
    }

    void AddSpecialCards(List<Card> cardList)
    {
        int totalCards = RuleVariables._totalCards;
        int totalRegularCards = RuleVariables._totalRegularCards;
        int dragonID = RuleVariables._dragonID;
        int majongId = RuleVariables._majongID;
        int phoenixID = RuleVariables._phoenixID;

        for (int i = totalRegularCards; i < totalCards; i++)
        {
            Card currentCard = ((GameObject)Instantiate(cardPrefab)).GetComponent<Card>();
            int newCardID = i;
            bool newCardIsWildcard = (i == phoenixID);
            int newCardNumericalValue = (i == dragonID)? RuleVariables._dragonValue : RuleVariables._specialCardNumericalValue;
            newCardNumericalValue = (i == majongId) ? RuleVariables._majongValue : RuleVariables._specialCardNumericalValue;
            CardSuit newCardSuit = CardSuit.Special;

            currentCard.data = new CardData(newCardID, newCardNumericalValue, newCardSuit, newCardIsWildcard);

            try
            {
                currentCard.ChangeSprite(SpecialCardSprites.ElementAt(i - totalRegularCards));
            }
            catch (System.IndexOutOfRangeException)
            {
                Debug.LogError("Card Sprite Array out of bounds");
            }

            currentCard.transform.SetParent(canvas);
            currentCard.transform.position = transform.position;

            cardList.Add(currentCard);
        }
    }
}
