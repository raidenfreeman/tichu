﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System;

public class Hand : MonoBehaviour
{

    #region Singleton
    //make it a singleton
    private static Hand instance;

    public static Hand Instance
    {
        get
        {
            if (instance == null)
                instance = GameObject.FindObjectOfType<Hand>();
            return instance;
        }
    }
    #endregion

    public List<Card> CardsInHand;
    public List<Card> SelectedCards;

    public string IsCombinationParameter;
    #region OnlyForTesting
    public GameObject cardPrefab;
    private void OnCardSelected(object sender, EventArgs e)
    {
        SelectedCards.Add(((MouseInteractions)sender).GetComponent<Card>());
    }

    private void OnCardDeselected(object sender,EventArgs e)
    {
        foreach(Card c in SelectedCards)
        {
            c.GetComponent<Animator>().SetBool(IsCombinationParameter, false);
        }
        SelectedCards.Remove(((MouseInteractions)sender).GetComponent<Card>());
    }

    void OnEnable()
    {
        //todo Replace this with event handler class' method call
        /*MouseInteractions.ObjectSelectedEvent += OnCardSelected;
        MouseInteractions.ObjectDeselectedEvent += OnCardDeselected;*/
    }

    void OnDisable()
    {
        //todo Replace this with event handler class' method call
        /*MouseInteractions.ObjectSelectedEvent -= OnCardSelected;
        MouseInteractions.ObjectDeselectedEvent -= OnCardDeselected;*/
    }

    CardAnalysis t;
    // Use this for initialization
    void Start()
    {
        t = new CardAnalysis();
        if (IsCombinationParameter == null)
            Debug.Log("FREAKOUT");

        /*
        //AddNewCard(8, CardSuit.Clubs, false, cardPrefab);
        //AddNewCard(-1, CardSuit.Special, true, cardPrefab);
        //AddNewCard(12, CardSuit.Clubs, false, cardPrefab);
        //AddNewCard(1, CardSuit.Clubs, false, cardPrefab);
        //AddNewCard(12, CardSuit.Spades, false, cardPrefab);
        CreateNewCard(3, CardSuit.Clubs, false, cardPrefab);
        //AddNewCard(2, CardSuit.Diamonds, false, cardPrefab);
        CreateNewCard(5, CardSuit.Hearts, false, cardPrefab);
        CreateNewCard(7, CardSuit.Clubs, false, cardPrefab);
        //AddNewCard(7, CardSuit.Diamonds, false, cardPrefab);
        CreateNewCard(2, CardSuit.Hearts, false, cardPrefab);
        CreateNewCard(6, CardSuit.Clubs, false, cardPrefab);
        CreateNewCard(1, CardSuit.Diamonds, false, cardPrefab);
        //AddNewCard(-1, CardSuit.Special, false, cardPrefab);
        CreateNewCard(-1, CardSuit.Special, true, cardPrefab);


        List<CardData> dataList = new List<CardData>();
        foreach (Card c in _CardsInHand)
        {
            dataList.Add(c.data);
        }

        TrickRecognition trickRecognizer;
        trickRecognizer = new TrickRecognition();

        Debug.Log(trickRecognizer.IsStraight(dataList, 5, 20, true, false, false));*/
    }

    //only for testing
    void CreateNewCard(int value, CardSuit suit, bool wildcard, GameObject cardPrefab)
    {
        Card newCard = ((GameObject)Instantiate(cardPrefab)).GetComponent<Card>();
        newCard.data = new CardData(0, value, suit, wildcard);
        CardsInHand.Add(newCard);
    }
    #endregion


    public void AddCards(List<Card> cards)
    {
        CardsInHand.AddRange(cards);
        foreach (Card c in cards)
            c.transform.SetParent(this.transform);
        //ArrangeCards();
    }

    void Update()
    {
        /*if (Input.GetKeyDown(KeyCode.G))
        {
            AnalysisResult a = new None(SelectedCards.Select(x=>x.data).ToList(),SelectedCards.Count, 0);
            foreach(Card c in SelectedCards)
            {
                Debug.Log("Value: " + c.data.NumericalValue + "\nSuit: " + c.data.Suit + "\nID: " + c.data.ID + "\nWildcard: " + c.data.IsWildcard);
            }
        }*/

        if (Input.GetKeyDown(KeyCode.U))
        {
            //TichuCombinations T;
            List<CardData> selectedCardData = new List<CardData>();
            foreach (Card c in SelectedCards)
            {
                selectedCardData.Add(c.data);
            }
            AnalysisResult res = t.AnalyzeCards(selectedCardData);
            Debug.Log(res.ToString());
            /*if (T != TichuCombinations.None)
            {
                foreach (Card c in SelectedCards)
                {
                    c.GetComponent<Animator>().SetBool(IsCombinationParameter, true);
                }
            }*/
        }
        if (Input.GetKeyDown(KeyCode.O))
        {
            Debug.Log("Holding " + CardsInHand.Count);
            foreach (Card c in CardsInHand)
            {
                Debug.Log(c.data.NumericalValue);
            }
        }
    }

    void ArrangeCards()
    {
        List<Card> cards = CardsInHand;
        int i = 0;
        foreach (Card c in cards)
        {
            Transform t = c.transform;
            //int a = 40 - (80 * i / (cards.Count - 1)); rotation
            t.SetParent(this.transform);
            t.localPosition = this.transform.position;
            t.localPosition = new Vector3(40 * (i - (cards.Count() * 0.5f)), 0, 0);
            t.rotation = Quaternion.identity;
            //t.localScale = Vector3.one;
            i++;
        }
    }

}
