﻿using UnityEngine;
using System.Collections;

public static class RuleVariables
{
    public const float _cardHeight = 72;

    public const int _totalRegularCards = 52;

    public const int _totalSpecialCards = 4;

    public const int _totalRegularCardSuits = 4;


    //MUST BE REPLACED WITH SINGLETON!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    public static RectTransform canvasRectTransform;

    public const int AceMaxValue = 14;

    public const int _totalCards = _totalRegularCards + _totalSpecialCards;

    public const int _cardsPerSuit = _totalRegularCards / _totalRegularCardSuits;


    public static int _totalPlayers = 4;

    public static int _totalTeams = 2;

    public static bool _timeLimitEnabled = false; //if you have a time limit for your actions

    public static int _playersPerTeam = 2;

    public const int _specialCardNumericalValue = -1;

    //Card Info

    public const int _dragonID = 0 + _totalRegularCards;

    public const int _majongID = 1 + _totalRegularCards;

    public const int _phoenixID = 2 + _totalRegularCards;

    public const int _dogID = 3 + _totalRegularCards;

    public const int _dragonValue = 10000;

    public const int _majongValue = 1;

    public const int _maxCardValue = 14;
}
