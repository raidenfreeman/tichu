﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Linq;
using System.Collections.Generic;

public enum CardSuit { Spades = 0, Clubs = 1, Hearts = 2, Diamonds = 3, Special = 4 };

[RequireComponent(typeof(Image))]
[RequireComponent(typeof(MouseInteractions))]

public class Card : MonoBehaviour
{
    public CardData data;

    public void ChangeSprite(Sprite sprite)
    {
        GetComponent<Image>().sprite = sprite;
    }
}

[ProtoBuf.ProtoContract]
public class CardData
{
    [ProtoBuf.ProtoMember(1)]
    public readonly int ID;// { get; private set; }
    [ProtoBuf.ProtoMember(2)]
    public readonly int NumericalValue;// { get; private set; }
    [ProtoBuf.ProtoMember(3)]
    public readonly CardSuit Suit;// { get; private set; }
    [ProtoBuf.ProtoMember(4)]
    public readonly bool IsWildcard;// { get; private set; }

    private CardData()
    {

    }

    public CardData(int id, int value, CardSuit suit, bool isWildcard = false)
    {
        ID = id;
        NumericalValue = value;
        Suit = suit;
        IsWildcard = isWildcard;
    }
    public void DebugCardData()
    {
        Debug.Log("-----------------");
        Debug.Log(ID);
        Debug.Log(NumericalValue);
        Debug.Log(Suit);
    }
}
