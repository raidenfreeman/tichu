﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;


/// <summary>
/// The abstract result of analysis describing the combination of cards
/// </summary>
public abstract class AnalysisResult
{

    //Warning: this class works only with 1 wildcard in each combination.

    public List<CardData> OrderedCards { get; protected set; }
    public readonly int CardCount;
    public int CombinationValue { get; protected set; }
    public int HighestCardValue { get; protected set; }
    public int LowestCardValue { get; protected set; }
    public int WildcardsCount { get; protected set; }

    /// <summary>
    /// Constructs a response of analysis
    /// </summary>
    /// <param name="cards">this class works only with 1 wildcard in each combination.</param>
    /// <param name="combination"></param>
    public AnalysisResult(List<CardData> cards, int cardCount, int wildcardsCount)
    {
        CardCount = cardCount;
        WildcardsCount = wildcardsCount;
        OrderedCards = InitializeOrderedCards(cards);
        CombinationValue = CalculateCombinationValue();
        HighestCardValue = CalculateHighestCardValue();
        LowestCardValue = CalculateLowestCardValue();
    }

    protected virtual List<CardData> InitializeOrderedCards(List<CardData> cards)
    {
        return cards.CopyList();
    }

    protected virtual int CalculateCombinationValue()
    {
        return CalculateHighestCardValue();
    }

    protected virtual int CalculateLowestCardValue()
    {
        return OrderedCards.First().NumericalValue;
    }

    protected virtual int CalculateHighestCardValue()
    {
        return OrderedCards.Last().NumericalValue;
    }

}

/// <summary>
/// Represents no valid combination
/// </summary>
public class None : AnalysisResult
{
    public None(List<CardData> cards, int cardCount, int wildcardsCount) : base(cards, cardCount, wildcardsCount)
    {

    }

    protected override int CalculateCombinationValue()
    {
        return 0;
    }

    protected override int CalculateHighestCardValue()
    {
        return 0;
    }

    protected override int CalculateLowestCardValue()
    {
        return 0;
    }

}


/// <summary>
/// Represents 3 cards of the same value with 2 cards of the same value
/// </summary>
public class FullHouse : AnalysisResult
{
    public readonly int TripleValue;
    public readonly int PairValue;
    public FullHouse(List<CardData> cards, int cardCount, int wildcardsCount) : base(cards, cardCount, wildcardsCount)
    {

    }

    /*public FullHouse(List<CardData> cards, int cardCount, int wildcardsCount, FullHouse previousTrick) : base(cards, cardCount, wildcardsCount)
    {
        TripleValue = previousTrick.TripleValue;
    }*/

    protected override List<CardData> InitializeOrderedCards(List<CardData> cards)
    {
        if (base.WildcardsCount == 0)
            return base.InitializeOrderedCards(cards);

        var groups = cards.GroupBy(x => x.NumericalValue).ToList();
        if (groups.Where(x=>x.Count() == 2) != null)
        {
            Debug.Log("2s");
            //HighestCardValue cards.Last()
        }
        return base.InitializeOrderedCards(cards);
    }

    //protected override int CalculateCombinationValue()
    //{
    /*
        int tripleValue, pairValue;
        var groups = OrderedCards.GroupBy(x => x.NumericalValue).ToList();
        if (groups.Count == 2)
        {
            tripleValue = groups.Where(x => x.Count() == 3).First().First().NumericalValue;
            pairValue = groups.Where(x => x.Count() == 2).First().First().NumericalValue;
        }
        else
        {
            //if it contains a wildcard, we have 3 groups
            var subgroups = groups.Where(x => x.Count() == 2);
            //we make 2 subgroups without the wildcard
            int pair1 = subgroups.First().First().NumericalValue;
            int pair2 = subgroups.ElementAt(1).First().NumericalValue;
            if (pair1 > pair2)
            {
                tripleValue = pair1;
                pairValue = pair2;
            }
            else
            {
                tripleValue = pair2;
                pairValue = pair1;
            }
        }
        return tripleValue * 10 * RuleVariables._maxCardValue + pairValue;*/
    //}
}

/// <summary>
/// Represents a single card
/// </summary>
public class Single : AnalysisResult
{
    public Single(List<CardData> cards, int cardCount, int wildcardsCount) : base(cards, cardCount, wildcardsCount)
    {
    }
}


/// <summary>
/// Represents a pair of cards with the same value
/// </summary>
public class Pair : AnalysisResult
{
    public Pair(List<CardData> cards, int cardCount, int wildcardsCount) : base(cards, cardCount, wildcardsCount)
    {
    }
}

/// <summary>
/// Represents a N amount of pairs of incremental (by 1) values
/// </summary>
public class NContPair : AnalysisResult
{
    public NContPair(List<CardData> cards, int cardCount, int wildcardsCount) : base(cards, cardCount, wildcardsCount)
    {
    }


}

/// <summary>
/// Represents 3 cards of the same value
/// </summary>
public class Triple : AnalysisResult
{
    public Triple(List<CardData> cards, int cardCount, int wildcardsCount) : base(cards, cardCount, wildcardsCount)
    {
    }
}


/// <summary>
/// Represents 5 or more cards with incremental values
/// </summary>
public class Straight : AnalysisResult
{
    public Straight(List<CardData> cards, int cardCount, int wildcardsCount) : base(cards, cardCount, wildcardsCount)
    {
    }
}


/// <summary>
/// Represents four cards of the same value, or a straight flush
/// </summary>
public class Bomb : AnalysisResult
{
    public Bomb(List<CardData> cards, int cardCount, int wildcardsCount) : base(cards, cardCount, wildcardsCount)
    {
    }

    protected override int CalculateCombinationValue()
    {
        return base.CardCount * RuleVariables._maxCardValue * 10 + base.HighestCardValue;
    }
}