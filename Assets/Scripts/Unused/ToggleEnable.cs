﻿using UnityEngine;
using System.Collections;

public class ToggleEnable : MonoBehaviour {
    public void ToggleEnabled()
    {
        gameObject.SetActive(!gameObject.activeInHierarchy);
    }
}
