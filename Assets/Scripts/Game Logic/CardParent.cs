﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/// <summary>
/// Responsible for tracking and moving Cards.
/// </summary>
public class CardParent
{
    /// <summary>
    /// Attempts to move a card, from its current owner to a new one
    /// </summary>
    /// <param name="card">The card to be moved</param>
    /// <param name="target">The new owner of the card</param>
    /// <returns>True if successful</returns>
    public bool TryMoveCard(Card card, object target)
    {
        /*************************************************
         * Idea:
         * Have a CardOwner type, that every card has one
         * This method changes the cardOwner
         *************************************************/

        //TODO Specify target's type
        //TODO Implement
        throw new NotImplementedException();
        return false;
    }

    /// <summary>
    /// Attempts to move multiple cards, from their current owner(s) to a new one
    /// </summary>
    /// <param name="cards">The list of cards to be moved</param>
    /// <param name="target">The new owner of the card</param>
    /// <returns>True if successful</returns>
    public bool TryMoveCards(IEnumerable<Card> cards, object target)
    {
        //TODO CARE!!!! Cards might have different owners. We need to verify that we have the right to move them!
        //TODO Implement
        throw new NotImplementedException();
        return false;
    }

    /// <summary>
    /// the number of cards active in the game
    /// </summary>
    public int NumberOfActiveCards { get { return ActiveCards.Count; } }

    /// <summary>
    /// The cards that have not yet been played
    /// </summary>
    public List<Card> ActiveCards { get; private set; }

    /// <summary>
    /// The cards that have been played so far
    /// </summary>
    public List<Card> PlayedCards { get; private set; }

    /// <summary>
    /// the number of cards that have been played in the game
    /// </summary>
    public int NumberOfPlayedCards { get { return PlayedCards.Count; } }


}
