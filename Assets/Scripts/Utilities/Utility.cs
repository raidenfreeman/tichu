﻿using UnityEngine;
using System.Collections.Generic;

public static class Utility
{
    /// <summary>
    /// Clamps rotation between min and max values for each axis
    /// </summary>
    /// <param name="rotation">The rotation to be clamped</param>
    /// <param name="minX">Minimum rotation around the X axis</param>
    /// <param name="maxX">Maximum rotation around the X axis</param>
    /// <param name="minY">Minimum rotation around the Y axis</param>
    /// <param name="maxY">Maximum rotation around the Y axis</param>
    /// <param name="minZ">Minimum rotation around the Z axis</param>
    /// <param name="maxZ">Maximum rotation around the Z axis</param>
    /// <returns>A rotation between min and max, for each axis respectively</returns>
    public static Quaternion ClampRotation(Quaternion rotation, float minX, float maxX, float minY, float maxY, float minZ, float maxZ)
    {
        if (minX > maxX || minY > maxY || minZ > maxZ)
            throw new System.ArgumentException();

        //convert negative angles to 0-360 range
        if (minX < 0)
            minX = 360 + minX;
        if (minY < 0)
            minY = 360 + minY;
        if (minZ < 0)
            minZ = 360 + minZ;
        if (maxX < 0)
            maxX = 360 + maxX;
        if (maxY < 0)
            maxY = 360 + maxY;
        if (maxZ < 0)
            maxZ = 360 + maxZ;

        float xAngle, yAngle, zAngle;

        xAngle = rotation.eulerAngles.x;
        yAngle = rotation.eulerAngles.y;
        zAngle = rotation.eulerAngles.z;

        if (xAngle > maxX && xAngle < minX)
            xAngle = (xAngle - maxX) > (minX - xAngle) ? minX : maxX;

        if (yAngle > maxY && yAngle < minY)
            yAngle = (yAngle - maxY) > (minY - yAngle) ? minY : maxY;

        if (zAngle > maxZ && zAngle < minZ)
            zAngle = (zAngle - maxZ) > (minZ - zAngle) ? minZ : maxZ;

        return Quaternion.Euler(xAngle, yAngle, zAngle);
    }

    /// <summary>
    /// Clamps rotation around all axes between <paramref name="minAngle"/> and <paramref name="maxAngle"/>
    /// </summary>
    /// <param name="rotation"></param>
    /// <param name="minAngle"></param>
    /// <param name="maxAngle"></param>
    /// <returns></returns>
    public static Quaternion ClampRotation(Quaternion rotation, float minAngle, float maxAngle)
    {
        return ClampRotation(rotation, minAngle, maxAngle, minAngle, maxAngle, minAngle, maxAngle);
    }

    /// <summary>
    /// Clamps the rotation of each axis between <paramref name="ange"/> and -<paramref name="ange"/>
    /// </summary>
    /// <param name="rotation">The rotation to clamp</param>
    /// <param name="angle">The max and min angle</param>
    public static Quaternion ClampRotation(Quaternion rotation, float angle)
    {
        return ClampRotation(rotation, -angle, angle, -angle, angle, -angle, angle);
    }

    /// <summary>
    /// Mirrors rotations around each axis
    /// </summary>
    /// <param name="rotation">The rotation</param>
    /// <returns>Returns the symmetrical rotation respective to that axis. x+180degrees, where x the rotation around each axis</returns>
    public static Quaternion MirrorRotation(Quaternion rotation, bool xAxis = true, bool yAxis = true, bool zAxis = true)
    {
        float xAngle, yAngle, zAngle;

        xAngle = rotation.eulerAngles.x;
        yAngle = rotation.eulerAngles.y;
        zAngle = rotation.eulerAngles.z;

        if (xAxis)
        {
            if (xAngle < 180)
                xAngle = 180 - xAngle;
            else
                xAngle = 360 + 180 - xAngle;
        }
        if (yAxis)
        {
            if (yAngle < 180)
                yAngle = 180 - yAngle;
            else
                yAngle = 360 + 180 - yAngle;
        }
        if (zAxis)
        {
            if (zAngle < 180)
                zAngle = 180 - zAngle;
            else
                zAngle = 360 + 180 - zAngle;
        }
        return Quaternion.Euler(xAngle, yAngle, zAngle);
    }

    /// <summary>
    /// Mirrors rotations ignoring the Z axis
    /// </summary>
    /// <param name="rotation">The rotation</param>
    /// <returns>Returns x+180degrees, where x the rotation around each axis</returns>
    public static Quaternion MirrorRotation2D(Quaternion rotation)
    {
        float xAngle, yAngle, zAngle;

        xAngle = rotation.eulerAngles.x;
        yAngle = rotation.eulerAngles.y;
        zAngle = rotation.eulerAngles.z;

        if (xAngle > 180)
            xAngle -= 180;
        else
            xAngle += 180;
        if (yAngle > 180)
            yAngle -= 180;
        else
            yAngle += 180;
        return Quaternion.Euler(xAngle, yAngle, zAngle);
    }
}

