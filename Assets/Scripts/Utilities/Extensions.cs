﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;

public static class Extensions {

    /// <summary>
    /// Shuffles a generic list
    /// </summary>
    /// <param name="list">The generic list</param>
    /// <remarks>Code provided by Rob Thijssen (stack overflow)</remarks>
    public static void Shuffle<T>(this IList<T> list)
    {
        RNGCryptoServiceProvider provider = new RNGCryptoServiceProvider();
        int n = list.Count;
        while (n > 1)
        {
            byte[] box = new byte[1];
            do provider.GetBytes(box);
            while (!(box[0] < n * (Byte.MaxValue / n)));
            int k = (box[0] % n);
            n--;
            T value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
    }

    /// <summary>
    /// Creates a deep copy of a list of CardData
    /// </summary>
    /// <param name="list">The list to copy</param>
    /// <returns>A new list with duplicate elements of the original</returns>
    public static List<CardData> CopyList(this List<CardData> list)
    {
        List<CardData> copy = new List<CardData>();
        foreach(CardData c in list)
        {
            copy.Add(c);
        }
        return copy;
    }
}
