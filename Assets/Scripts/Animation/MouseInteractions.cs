﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.EventSystems;

//public delegate void ObjectSelectionDelegate(object sender);

[RequireComponent(typeof(Animator))]
public class MouseInteractions : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IBeginDragHandler, IEndDragHandler, IPointerClickHandler
{
    public event EventHandler ObjectSelectedEvent;
    public event EventHandler ObjectDeselectedEvent;

    //public bool IsInteractible;
    [SerializeField]
    private Animator AnimatorReference;
    [SerializeField]
    private CanvasGroup CanvasGroupReference;

    [SerializeField]
    private string IsDraggedParameter;
    [SerializeField]
    private string IsSelectedParameter;
    [SerializeField]
    private string MouseOverParameter;
    [SerializeField]
    private RectTransform canvasRectTransform;

    [SerializeField]
    private MouseInteractionEventBroadcaster eventBroadcasterReference;


    void Start()
    {
        CheckEditorParameters();
        InitializePrivateVariables();
    }

    private void InitializePrivateVariables()
    {
        originalPosition = transform.localPosition;
    }


    /// <summary>
    /// Check every parameter expecting initialization from the editor
    /// </summary>
    private void CheckEditorParameters()
    {
        if (CanvasGroupReference == null)
        {
            //try to get this object's CanvasGroup component if it's missing
            if ((CanvasGroupReference = this.GetComponent<CanvasGroup>()) == null)
            {
                Debug.LogError("CanvasGroup Missing");
            }
        }
        if (canvasRectTransform == null)
        {
            //try to find a canvas if it's missing
            GameObject temp;
            if (temp = GameObject.FindWithTag("Canvas"))
            {
                canvasRectTransform = temp.GetComponent<RectTransform>();
            }
            else
            {
                Debug.LogError("Canvas missing Tag!");
            }
            if (canvasRectTransform == null)
            {
                Debug.LogError("Canvas missing RectTransform!");
            }
        }
        if (AnimatorReference == null)
        {
            //try to get this object's animator
            if ((AnimatorReference = this.GetComponent<Animator>()) == null)
            {
                Debug.LogError("Animator Reference missing!");
            }
        }
    }

    /// <summary>
    /// The parent the object had before it got dragged out.
    /// </summary>
    /// <remarks>
    /// In case the dragged object does not drop on something valid, it has to return to its original position and parent
    /// </remarks>
    private Transform parentToReturnTo;

    /// <summary>
    /// The position in the parent that the object was, when dragging began
    /// </summary>
    private int siblingIndex;
    public void OnBeginDrag(PointerEventData d)
    {
        /*if (!CanvasGroupReference.interactable)
            return;*/
        CanvasGroupReference.blocksRaycasts = false;//stop the canvas group from receiving raycasts

        parentToReturnTo = this.transform.parent;//remember current parent

        siblingIndex = this.transform.GetSiblingIndex();//and position inside that parent's group

        this.transform.SetParent(canvasRectTransform);//set the parent to the global canvas while being dragged
        try { AnimatorReference.SetBool(IsDraggedParameter, true); }
        catch (System.NullReferenceException) { Debug.LogError("missing animator reference"); }
        StopAllCoroutines();
        StartCoroutine(SmoothFollowCoroutine());
    }

    public float lerpStep = 0.2f;
    public float lookAtDepth = 20;
    public IEnumerator SmoothFollowCoroutine()
    {
        Vector3 mousePositionWorld;
        while (AnimatorReference.GetBool(IsDraggedParameter))
        {
            RectTransformUtility.ScreenPointToWorldPointInRectangle(canvasRectTransform, Input.mousePosition, Camera.main, out mousePositionWorld);
            if (((Vector2)transform.position - (Vector2)mousePositionWorld).sqrMagnitude > 0.01f)
            {
                //force z to stay the same
                float previousZ = transform.position.z;
                transform.position = Vector2.Lerp(transform.position, mousePositionWorld, lerpStep);
                transform.position = new Vector3(transform.position.x, transform.position.y, previousZ);

                transform.LookAt(mousePositionWorld + (Vector3.forward * (lookAtDepth)));
                //force limits to rotation
                transform.localRotation = Utility.ClampRotation(transform.localRotation, -60, 60, -40, 40, 0, 0);
            }
            else
            {
                float previousZ = transform.position.z;
                transform.position = mousePositionWorld;
                transform.position = new Vector3(transform.position.x, transform.position.y, previousZ);
                transform.rotation = Quaternion.identity;
            }
            yield return new WaitForSeconds(0.01f);
        }
    }

    public void OnEndDrag(PointerEventData d)
    {
        GameObject raycastResult = d.pointerCurrentRaycast.gameObject;

        bool isDropZone = false, isCard = false;
        bool didRaycastHit = raycastResult != null;
        if (didRaycastHit)
        {
            isDropZone = raycastResult.GetComponent<CardDropZone>() != null;
            isCard = raycastResult.GetComponent<Card>() != null;
            if (isDropZone)
            {
                parentToReturnTo = null;
                this.transform.SetParent(raycastResult.transform);
                Transform firstChild = raycastResult.transform.GetChild(0);
                if (firstChild != null && firstChild.position.x > this.transform.position.x)
                    this.transform.SetAsFirstSibling();
            }
            if (isCard)
            {
                parentToReturnTo = null;
                this.transform.SetParent(raycastResult.transform.parent);
                if (this.transform.position.x > raycastResult.transform.position.x)
                    this.transform.SetSiblingIndex(raycastResult.transform.GetSiblingIndex() + 1);
                else
                    this.transform.SetSiblingIndex(raycastResult.transform.GetSiblingIndex());
            }
        }
        if (!didRaycastHit || !(isDropZone || isCard))
        {
            this.transform.SetParent(parentToReturnTo);
            this.transform.SetSiblingIndex(siblingIndex);
            parentToReturnTo = null;
        }
        CanvasGroupReference.blocksRaycasts = true;
        AnimatorReference.SetBool(IsDraggedParameter, false);
        transform.localRotation = Quaternion.identity;
    }

    private void Select()
    {
        originalPosition = transform.localPosition;
        //todo Replace this with event handler class' method call
        //ObjectSelectedEvent(this,null);
        AnimatorReference.SetBool(IsSelectedParameter, true);
        StartCoroutine(SelectionMovementCoroutine());
    }
    private void Deselect()
    {
        //todo Replace this with event handler class' method call
        //ObjectDeselectedEvent(this,null);
        AnimatorReference.SetBool(IsSelectedParameter, false);
        StartCoroutine(DeselectionMovementCoroutine());
    }

    private Vector3 originalPosition;
    public IEnumerator DeselectionMovementCoroutine()
    {
        Vector3 targetLocation = originalPosition;
        while (!AnimatorReference.GetBool(IsSelectedParameter) && (transform.localPosition - targetLocation).sqrMagnitude > 0.01f)
        {
            transform.localPosition = Vector2.Lerp(transform.localPosition, targetLocation, lerpStep);
            yield return new WaitForSeconds(0.01f);
        }
    }

    private const float distance = 67;
    public IEnumerator SelectionMovementCoroutine()
    {
        Vector3 targetLocation = new Vector3(0, distance) + originalPosition;
        while (AnimatorReference.GetBool(IsSelectedParameter) && (transform.localPosition - targetLocation).sqrMagnitude > 0.01f)
        {
            transform.localPosition = Vector2.Lerp(transform.localPosition, targetLocation, lerpStep);
            yield return new WaitForSeconds(0.01f);
        }
    }

    public void OnPointerClick(PointerEventData d)
    {
        //if you're dragging, don't do anything.
        if (AnimatorReference.GetBool(IsDraggedParameter))
            return;
        if (AnimatorReference.GetBool(IsSelectedParameter) == false)
            Select();
        else
            Deselect();
    }

    public void OnPointerExit(PointerEventData p)
    {
        AnimatorReference.SetBool(MouseOverParameter, false);
    }

    public void OnPointerEnter(PointerEventData p)
    {
        AnimatorReference.SetBool(MouseOverParameter, true);
    }

}
