﻿using UnityEngine;
using System.Collections;

public class moveWithkeyboard : MonoBehaviour
{

    public SmoothFollow s;

    // Use this for initialization
    void Start()
    {
        if (s != null)
        {
            s.StoppedFollowing += S_StoppedFollowing;
            s.StartedFollowing += S_StartedFollowing;
        }
    }

    private void S_StartedFollowing(object sender, System.EventArgs e)
    {
        Debug.Log("Started following!");
        isFollowing = true;
    }

    private void S_StoppedFollowing(object sender, System.EventArgs e)
    {
        Debug.Log("Hurray stopped following!");
        isFollowing = false;
    }

    bool isFollowing = true;
    // Update is called once per frame
    void Update()
    {
        float verticalMovement = 0f;
        if (Input.GetKey(KeyCode.E))
            verticalMovement = 0.5f;
        if (Input.GetKey(KeyCode.Q))
            verticalMovement = -0.5f;
        if (Input.GetKey(KeyCode.E) && Input.GetKey(KeyCode.Q))
            verticalMovement = 0f;
        transform.Translate(new Vector3(Input.GetAxis("Horizontal")*0.5f, verticalMovement, Input.GetAxis("Vertical") * 0.5f));
        if(Input.GetKeyDown(KeyCode.P))
        {
            if (isFollowing)
                s.StopFollowing();
            else
                s.TryStartFollowingTarget(this.transform, 0.02f, true);
        }
    }
}
